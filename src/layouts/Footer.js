import React from "react"

const Footer = () =>{
  return (
    <footer>
      <h5>Copyright &copy; 2020 by Sanbercode</h5>
    </footer>
  )
}

export default Footer
